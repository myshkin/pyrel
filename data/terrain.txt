[
{"name": "permanent wall", 
"display": {"ascii": {"color": "UMBER", "symbol": "#"}}, 
"interactions": [{"action": "tunnel", "procs": [{"messageString": "This wall appears to be made of solid titanium.", "name": "print message"}]}], 
"flags": ["OBSTRUCT"]},

{"name": "granite wall", 
"display": {"ascii": {"color": "L_SLATE", "symbol": "#"}}, 
"interactions": [{"action": "tunnel", "procs": [{"difficulty": 2, "name": "tunnel"}]}], 
"flags": ["OBSTRUCT"]},

{"name": "door", 
"display": {"ascii": {"color": "UMBER", "symbol": "+"}}, 
"interactions": [{"action": "open", "procs": [{"messageString": "<2> opened the door.", "name": "print smart message"}, {"name": "replace terrain", "newTerrain": "open door"}]}], 
"flags": ["OBSTRUCT"]},

{"name": "down staircase", 
"display": {"ascii": {"color": "L_SLATE", "symbol": ">"}}, 
"interactions": [{"action": "descend", "procs": [{"messageString": "<2> enter a maze of down staircases.", "name": "print smart message"}, {"newLevel": "+1", "name": "change level"}]}]},

{"name": "up staircase", 
"display": {"ascii": {"color": "L_SLATE", "symbol": "<"}}, 
"interactions": [{"action": "ascend", "procs": [{"messageString": "<2> enter a maze of up staircases.", "name": "print smart message"}, {"newLevel": "-1", "name": "change level"}]}]},

{"name": "open door", 
"display": {"ascii": {"color": "UMBER", "symbol": "'"}}, 
"interactions": [{"action": "close", "procs": [{"messageString": "<2> closed the door.", "name": "print smart message"}, {"name": "replace terrain", "newTerrain": "door"}]}]},

{"name": "decorative floor tile", 
"display": {"ascii": {"color": "BLUE_SLATE", "symbol": "."}}}]
