# Wizard-mode commands
#
# These commands are specifically used for "wizard-mode" debugging, and
# are those which do not duplicate normal game functionality

import commands
import container
import gui
import mapgen.gameMap
import things.items.itemAllocator
import things.items.itemLoader

## This Command creates a random item.
class CreateItemCommand(commands.Command):
    def __init__(self, *args):
        commands.Command.__init__(self, *args)


    def execute(self):
        lootTemplate = things.items.itemLoader.getLootTemplate("kitchen sink")
        lootTemplate.resolveValues(self.gameMap.mapLevel)
        allocator = things.items.itemAllocator.ItemAllocator(self.gameMap.mapLevel, lootTemplate)
        item = allocator.allocate(self.gameMap, self.gameMap.getContainer(self.subject.pos))
        gui.messenger.message("Generated %s." % item.getShortDescription())
        item.pos = self.subject.pos



## This Command jumps straight to a particular level, prompting for which
class JumpLevelCommand(commands.Command):
    def __init__(self, *args):
        commands.Command.__init__(self, *args)


    def contextualizeAndExecute(self):
        self.newLevel = yield gui.prompt.resolvePrompt(
                gui.prompt.NumericPrompt(message='Which level? '))
        self.gameMap.makeLevel(self.newLevel)



## Test command
# This is a placeholder for testing other parts of the command and prompt
# objects. It's here so that you don't have to create a test framework and
# command structure just to test something.
#
# Feel free to change this to test anything you like
class TestCommand(commands.Command):
    def __init__(self, *args):
        commands.Command.__init__(self, *args)


    def contextualizeAndExecute(self):
        test = yield gui.prompt.resolvePrompt(
                gui.prompt.TextPrompt(message='Enter text: '))
        print 'Text entered was: %s' % test



## Central entry-point for all the debugging or "wizard-mode" commands
# Prompts for a further command key, and looks it up in a separate
# command set.
class WizardCommand(commands.Command):
    def __init__(self, *args):
        commands.Command.__init__(self, *args)


    def contextualizeAndExecute(self):
        player = self.gameMap.getContainer(container.PLAYERS)[0]
        if not player.hasUsedDebugCommands:
            isOk = yield gui.prompt.resolvePrompt(
                    gui.prompt.YesNoPrompt(message='You are about to use the dangerous and unsupported debug commands. This will permanently taint your character. Ok to continue?'))
            if not isOk:
                return
            player.hasUsedDebugCommands = True
        debugInput = yield gui.prompt.resolvePrompt(
                gui.prompt.CommandPrompt(message="Debug Command:", commandSet='wizard'))
        debugCommand = commands.inputToCommandClassMap[debugInput](
                player, debugInput, self.gameMap )
        yield debugCommand
