## This module handles drawing animations for various effects. Each of the 
# drawFoo functions is a generator function, and is expected to yield a
# list of lists of (position, symbol, color) tuples describing what to draw on
# each frame of the animation.
import gui
import util.geometry

## This decorator function causes the given function to be registered with 
# our display layer.
def register(func):
    def wrappedFunc(*args, **kwargs):
        generator = func(*args, **kwargs)
        gui.animation.receiveAnimation(generator)
    return wrappedFunc


## Send a projectile from the start position to the end position.
@register
def drawProjectile(start, end, displayData):
    symbol = displayData['ascii']['symbol']
    color = displayData['ascii']['color']
    tiles = util.geometry.getLineBetween(start, end)
    for tile in tiles:
        yield [(tile, symbol, color)]

