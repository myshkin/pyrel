## This module handles various geometry tasks, like finding lines or circles.

import copy
import math
import random

## Given a tuple specifying a direction, change it to one 90 degrees away
#  The parameter clockwise specifies the chance of moving in a clockwise
#  direction, set it to 0 or 100 if you want it determined which direction
#  to turn
def changeCardinalDirection(direction, clockwise = 50):

    if len(direction) != 2:
        raise RuntimeError("Directions must have two elements")
    
    # If we move 90 degrees, x and y swap
    yDir = direction[0]
    xDir = direction[1]
    
    isClockwise = (random.randint(1, 100) <= clockwise)
    
    # Factor for multiplication
    mult = [-1, 1][isClockwise]
    
    if direction[0]: 
        return(xDir, yDir * mult)
    if direction[1]: 
        return(xDir * -1 * mult, yDir)
    
    # you entered a null direction direction coordinate
    return (0, 0)
        

## Return Euclidean distance between two points
def distance(a, b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)
        
        
## Given a start point and an end point pick the proper cardinal direction that will
#  get us closest to our target (in physics parlance, minimal impact parameter)
#  The return is xDirection, yDirection, one of which is 0 and the other is either 
#  1 or -1.
def getCardinalDirection(start, end):
    x1, y1 = start
    x2, y2 = end
    dx = abs(x2 - x1)
    dy = abs(y2 - y1)
    
    # Target lies to the east
    if (x2 >= x1) and (dx >= dy): 
        return (1, 0)
    # Target lies to the west
    if (x2 < x1) and (dx >= dy): 
        return (-1, 0)
    # Target lies to the south
    if (y2 >= y1) and (dy > dx): 
        return (0, 1)
    # Target lies to the north
    if (y2 < y1) and (dy > dx): 
        return (0, -1)
    
    # You moron, you passed in the same point for start and end
    return (0, 0)
    

## Given a set of locations, return a random cell from that set that's close to the target
#  /todo enable the closeness parameter specifies how many close cells to include.  
#  I.e. if closeness is 5 it will return a random location in the top 5 closest. 
#  If closeness is <= 1 it will return the closest cell. 
def getNearbyLocation(locationSet, target, closeness = 1):
    if not locationSet: 
        return
    locationList = list(locationSet)
    closest = locationList[0]
    closestDistance = distance(locationList[0], target)
    for loc in locationList:
        if distance(loc, target) < closestDistance:
            closest = loc
            closestDistance = distance(loc, target)
    return closest
    
    
## Given a location and a cardinal direction, find the squares that are
#  parallel to the given square with given offset.
def getParallelCells(location, direction, offset = 1):
    x, y = location
    cwDir = changeCardinalDirection(direction, clockwise = 100)
    ccwDir = changeCardinalDirection(direction, clockwise = 0)
    x1 = x + cwDir[0] * offset
    x2 = x + ccwDir[0] * offset
    y1 = y + cwDir[1] * offset
    y2 = y + ccwDir[1] * offset
    return (x1, y1), (x2, y2)
    
    
## Return a list of (x, y) tuples describing all of the tiles between the 
# start tile and the end tile. Uses Bresenham's algorithm, as adapted from:
# http://www.gamedev.net/page/resources/_/technical/game-programming/line-drawing-algorithm-explained-r1275
def getLineBetween(start, end):
    result = []
    x1, y1 = start
    x2, y2 = end
    dx = abs(x2 - x1)
    dy = abs(y2 - y1)
    x = x1
    y = y1
    
    if x2 >= x1:
        xInc1 = 1
        xInc2 = 1
    else:
        xInc1 = -1
        xInc2 = -1
        
    if y2 >= y1:
        yInc1 = 1
        yInc2 = 1
    else:
        yInc1 = -1
        yInc2 = -1

    if dx >= dy:
        xInc1 = 0
        yInc2 = 0
        denominator = dx
        numerator = dx / 2
        numAdd = dy
        numPoints = dx
    else:
        xInc2 = 0
        yInc1 = 0
        denominator = dy
        numerator = dy / 2
        numAdd = dx
        numPoints = dy
    
    for i in xrange(numPoints):
        result.append((x, y))
        numerator += numAdd
        if numerator >= denominator:
            numerator -= denominator
            x += xInc1
            y += yInc1
        x += xInc2
        y += yInc2
    return result


## Check if directionTwo is 90 degrees clockwise rotation from
#  directionOne.  /todo make this more general.
def isClockwise(directionOne, directionTwo):
    testDirection = tuple(directionOne)
    testDirection = changeCardinalDirection(testDirection, clockwise = 100)
    return testDirection == directionTwo
    
    
## These values are arrays of preferred directions for the
#  marching squares algorithm (see below)
#  The zero algorithm should never occur and should probably
#  return something for failure.  For debug purposes we'll
#  use it
marchingSquaresDirectionCounterClockwise = [
        (1, 0),    (1, 0),  (0, 1), (1, 0),
        (0, -1),  (0, -1),  (0, -1),  (0, -1),
        (-1, 0), (-1, 0), (0, 1), (1, 0), 
        (-1, 0), (-1, 0), (0, 1), (-1, 0)]
 
marchingSquaresDirectionClockwise = [
        (-1, 0), (0, 1), (-1, 0), (-1, 0),
        (1, 0),   (0, 1), (0, 1), (-1, 0),
        (0, -1),   (1, 0),  (0, -1),  (0, -1), 
        (1, 0),   (0, 1), (1, 0), (1, 0)]
        
marchingSquaresExitDirections = [
        set(), set(), set(), {(0, 1)},
        set(), {(1, 0)}, set(), {(1, 0), (0, 1)},
        set(), set(), {(-1, 0)},{(-1, 0), (0, 1)},
        {(0, -1)}, {(1, 0), (0, -1)}, {(-1, 0), (0, -1)},
        {(0, 1), (1, 0), (-1, 0), (0, -1)}]    
    
    
## Get the marching square suggested direction used for following around
#  obstacles of arbitrary shape.  (see algorithm at
#  http://en.wikipedia.org/w/index.php?title=Marching_squares&oldid=312115449
# 
#  The input is a 2 x 2 matrix with boolean values.  Squares are FALSE if they
#  are blocked off and TRUE if they are available to place a feature.
#  Grid ordering is as follows
# 8 4
# 2 1
#  The function returns a suggestedDirection to move the block and a possibly empty tuple
#  of exitDirections which can be used to break out of the contour following
#  loop.
def marchingSquare(booleanMatrix, clockwise = False):
    # First convert the array into an integer value
    key = 0
    if booleanMatrix[0][0]:
        key += 8
    if booleanMatrix[1][0]:
        key += 4
    if booleanMatrix[0][1]:
        key += 2
    if booleanMatrix[1][1]:
        key += 1
     
    if clockwise:
        suggestedDirection = marchingSquaresDirectionClockwise[key]
    else:
        suggestedDirection = marchingSquaresDirectionCounterClockwise[key]

    exitDirections = marchingSquaresExitDirections[key]
    
    if key == 0:
        # This should never occur, but for debugging reasons we'll 
        # allow it for now.
        print "Fully impenetrable array in marchingSquares"
        #raise RuntimeError("Full obstacle matrix passed")
    
    return suggestedDirection, exitDirections

    
## Reverse direction, works for arbitrary x, y direction, not
#  just cardinal ones
def reverseDirection(direction):
    if len(direction) != 2:
        raise RuntimeError("Directions must have two elements")
    return (direction[0] * -1, direction[1] * -1)