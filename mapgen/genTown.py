## This module will handle town generation.  For now it just makes
# a debug level that is entirely hardcoded for use in debugging.

import container
import gameMap
import generationMap
import genUtility
import things.terrain.terrainLoader
import things.creatures.player
import things.creatures.creatureLoader
import things.creatures.creatureAllocator
import things.items.itemLoader
import things.items.itemAllocator

def makeTownLevel(curMap, width, height):
    # make a generation map (which we won't use for now)
    genMap = generationMap.GenerationMap(width, height)
    
    # Make the surrounding ring of permanent walls
    genUtility.makeRectangleHollow(curMap, genMap, 0, 0, width - 1 , height - 1, 
            'permanent wall', priority = 10)
            
    # Clear everything else (maybe not necessary?)
    genUtility.makeRectangleFilled(curMap, genMap, 1, 1, width - 3, height - 3,
            None)
    
    things.terrain.terrainLoader.makeTerrain('down staircase', curMap, 
            (1, 1), 0)
    things.terrain.terrainLoader.makeTerrain('decorative floor tile', curMap, 
            (10, 10), 0)
    things.terrain.terrainLoader.makeTerrain('door', curMap, (9, 10), 0)
    things.terrain.terrainLoader.makeTerrain('door', curMap, (8, 10), 0)

    things.creatures.creatureLoader.makeCreature("Filthy street urchin", curMap, (6, 6))
    things.items.itemLoader.makeItem(('sword', '& Dagger~'), 0, curMap, (3, 4))
    things.items.itemLoader.makeItem(('sword', '& Dagger~'), 0, curMap, (3, 6))
    things.items.itemLoader.makeItem(('amulet', 'Devotion'), 80, curMap, (2, 4))
    things.items.itemLoader.makeItem(('ring', 'Prowess'), 80, curMap, (2, 5))
    things.items.itemLoader.makeItem(('ring', 'Finesse'), 80, curMap, (1, 5))
    things.items.itemLoader.makeItem(('ring', 'Slaying'), 80, curMap, (3, 5))
    things.items.itemLoader.makeItem(('spike', '& Iron Spike~'), 0, curMap, (3, 3))
    things.items.itemLoader.makeItem(('potion', 'Cure Light Wounds'), 0, curMap, (3, 2))
    things.items.itemLoader.makeItem(('potion', 'Speed'), 0, curMap, (3, 2))
    things.items.itemLoader.makeItem(('food', '& Ration~ of Food'), 0, curMap, (3, 6))
    things.items.itemLoader.makeItem(('container', '& Small wooden chest~'), 0, 
            curMap, (5, 4))
    quiver = things.items.itemLoader.makeItem(('container', '& quiver~'), 0, curMap, (5, 5))
    arrows = things.items.itemLoader.makeItem(('arrow', '& Arrow~'), 0, curMap, (5, 6))
    quiver.addItemToInventory(arrows)
    curMap.getContainer((5, 6)).unsubscribe(arrows)
    player = curMap.getContainer(container.PLAYERS)[0]
    player.pos = (4, 4)
    curMap.addSubscriber(player, (4, 4))
    

    # A test pattern to explore.
    for x in xrange(20, width - 20, 4):
        for y in xrange(20, height - 20, 4):
            wall = things.terrain.terrainLoader.makeTerrain('granite wall', curMap, (x, y), 0)
            wall.display = {'ascii': {'color': (255, 0, 0), 'symbol': '#'}}
            wall = things.terrain.terrainLoader.makeTerrain('granite wall', curMap, (x + 1, y), 0)
            wall.display = {'ascii': {'color': (0, 255, 0), 'symbol': '#'}}
            wall = things.terrain.terrainLoader.makeTerrain('granite wall', curMap, (x, y + 1), 0)
            wall.display = {'ascii': {'color': (0, 0, 255), 'symbol': '#'}}
            wall = things.terrain.terrainLoader.makeTerrain('granite wall', curMap, (x + 1, y + 1), 0)
            wall.display = {'ascii': {'color': (255, 255, 0), 'symbol': '#'}}
