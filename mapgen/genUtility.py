## This module includes all the basic functions for dungeon generation
# including basic terrain handling, room formation, and interfacing
# with the generationMap

# Please keep all functions in alphabetical order

import container
import gameMap
import generationMap
import things.terrain.terrainLoader
import things.creatures.player
import things.creatures.creatureLoader
import things.creatures.creatureAllocator
import things.items.itemLoader
import things.items.itemAllocator
import util.geometry

import random


## Destroy all terrain in the specified cell. Except that we treat differently
# anything whose "pos" field is None, since that indicates that the Thing in
# question is an alias of a Thing, not a real one (for performance purposes). 
# So we remove the alias instead of destroying the Thing.
def clearCell(curMap, x, y):
    things = curMap.getContainer((x, y), container.TERRAIN)
    for thing in list(things):
        if thing.pos is None:
            curMap.removeSubscriber(thing, (x, y))
        else:
            curMap.destroy(thing)
            

## Draw a corridor through the map connecting the two provided endpoints.
def clearCorridor(curMap, genMap, start, end):
    print "making corridor"
    print "start",start
    print "end",end
    isHorizontal = True
    curX, curY = start
    xDir = 1 if curX < end[0] else -1
    yDir = 1 if curY < end[1] else -1
    while (curX, curY) != end:
        clearCell(curMap, curX, curY)
        # Force horizontality if we're at the target Y coordinate already, 
        # vice versa for verticality; if neither holds true then we flip a 
        # coin.
        isHorizontal = (curY == end[1]) or (curX != end[0] and random.random() > .5)
        if isHorizontal:
            curX += xDir
        else:
            curY += yDir


## See if a rectangle is placeable in the grid
def isRectanglePlaceable(genMap, xWest, yNorth, width, height, priority):
    # If this is slow we can speed it up by not checking every cell
    for x in xrange(xWest, xWest + width):
        for y in xrange(yNorth, yNorth + height):
            if genMap.grid[x][y].priority >= priority: 
                return False
    return True
    

## Make a tunnel adjacent to an obstacle.  Stop if you hit something
#  This predates the marchingSquares algorithm.  It's currently unused
def makeAdjacentTunnel(curMap, genMap, x, y, tunnelDirection, obstacleDirection, 
    priority = 1):
    # we check that we can make a tunnel here, the obstacle still
    # exists in the obstacle direction and is not a pierceable location
    while (genMap.isHigherPriority(x, y, priority) and 
            (not genMap.isHigherPriority(x + obstacleDirection[0], y + obstacleDirection[1], priority)) 
            and (not genMap.isPierceable(x + obstacleDirection[0], y + obstacleDirection[1]))):
        # clear the cell and set the tunnel
        clearCell(curMap, x, y)
        genMap.setTunnel(x, y, True)
        # This will fail on the obstacle side because of priority reasons
        genMap.setParallelWallsPierceable(x, y, tunnelDirection, True, priority = priority)
        x = x + tunnelDirection[0]
        y = y + tunnelDirection[1]
        print "tunnel x y",x,y
    return x, y    
    
    
## Create a rectangle of the designated terrain type.
# This can be used for the surrounding walls of rooms
# or for the border of permanent walls around a level
#
# \todo allow the option to avoid setting the corners, this will prevent
# rooms from connecting through the corner cell.
def makeRectangleHollow(curMap, genMap, xWest, yNorth, 
        width, height, terrainType, priority = 0, isPierceable = False,
        isRoom = False, doErasePrevious = True):
    # Maybe a paranoia bounds check?
    
    # Load up the terrain
    if terrainType is not None:
        terrainFactory = things.terrain.terrainLoader.getTerrainFactory(terrainType)
        terrain = terrainFactory.makeTerrain(curMap, None, 0)
    
    # Some helpful definitions
    xEast = xWest + width - 1
    ySouth = yNorth + height - 1
    
    # Debugging statements
    #print "Hollow Rectangle", terrainType
    #print "xWest, xEast", xWest, xEast + 1
    #print "yUp, YDown", yNorth, ySouth + 1
    
    # Make the top and bottom rows
    for x in xrange(xWest, xEast + 1):
        if terrainType is None:
            clearCell(curMap, x, yNorth)
            clearCell(curMap, x, ySouth)
        else:
            # Granite walls + permanent walls were being displayed as granite walls
            # Is this a bug?  For now we're going to clear cells when placing
            # terrain with a keyword.
            if doErasePrevious:
                clearCell(curMap, x, yNorth)
                clearCell(curMap, x, ySouth)
            curMap.addSubscriber(terrain, (x, yNorth))
            curMap.addSubscriber(terrain, (x, ySouth))
        
        genMap.setGridInfo(x, yNorth, priority = priority, 
                isPierceable = isPierceable, isRoom = isRoom)
        genMap.setGridInfo(x, ySouth, priority = priority,  
                isPierceable = isPierceable, isRoom = isRoom) 

    
    # Make the West and East columns
    for y in xrange(yNorth + 1, ySouth):
        if terrainType is None:
            clearCell(curMap, xWest, y)
            clearCell(curMap, xEast, y)
        else:
            if doErasePrevious:
                clearCell(curMap, xWest, y)
                clearCell(curMap, xEast, y)
            curMap.addSubscriber(terrain, (xWest, y))
            curMap.addSubscriber(terrain, (xEast, y))            

        genMap.setGridInfo(xWest, y, priority = priority, 
                isPierceable = isPierceable, isRoom = isRoom)
        genMap.setGridInfo(xEast, y, priority = priority, 
                isPierceable = isPierceable, isRoom = isRoom)
        
        
## Create a filled rectangle of the designated terrain type.                
def makeRectangleFilled(curMap, genMap, xWest, yNorth,
        width, height, terrainType, priority = 0, isRoom = False,
        doErasePrevious = True):
    # Probably should put a bounds check here
    
    # Debugging statements
    #print "FilledRectangle", terrainType
    #print "xWest, xEast", xWest, xWest + width - 1
    #print "yUp, yDown", yNorth, yNorth + height - 1
    
    # Load up the terrain
    if terrainType is not None:
        terrainFactory = things.terrain.terrainLoader.getTerrainFactory(terrainType)
        terrain = terrainFactory.makeTerrain(curMap, None, 0)
        
    for x in xrange(xWest, xWest + width):
        for y in xrange(yNorth, yNorth + height):
            if terrainType is None:
                clearCell(curMap, x, y)
            else:
                # see note in makeRectangleHollow
                if doErasePrevious:
                    clearCell(curMap, x, y)
                curMap.addSubscriber(terrain, (x, y))
            
            genMap.setGridInfo(x, y, priority = priority, isRoom = isRoom)


## Make a tunnel from a starting point aiming for a target.           
#  Stop when we encounter anything of note and return the square
#  that we hit a pierceable square (obstacle avoidance in here)   
# 
#  turnPct is the percentage chance that a tunnel turns at a given square.
#  randDirectionPct is the percentage chance that a turn goes off in a random
#  direction.  Otherwise the turn is so that the tunnel heads in the proper direction.
#  The default values are Angband style tunnels.
#
#  Caution: high priority tunnels will barrel through anything.
#
#  We return the x and y coordinates of the tunnel endpoint and the direction we
#  were last heading        
def makeTunnel(curMap, genMap, start, target, turnPct = 30, randDirectionPct = 10,
        priority = 1, minTunnelLength = 1):

    x = start[0]
    y = start[1]
    tunnelLength = 0
    print "making tunnel", start, target
    
    # Get the direction
    direction = util.geometry.getCardinalDirection(start, target)
 
    # If we're in a room but not a pierceable square, find the edge
    if genMap.isRoom(x, y) and not genMap.isPierceable(x, y): 
        x, y, direction = genMap.wanderToRoomEdge(x, y, direction)

    # We're now either at a pierceable cell or a random cell in the middle
    # of nowhere (somehow). Clear the wall, and set it to a junction square
    clearCell(curMap, x, y)
    
    # Remove pierceable settings from the tile locations 90 degrees to
    # ensure no other rooms can enter here.
    genMap.setParallelWallsPierceable(x, y, direction, False)
    
    # Add square as a junction
    genMap.addJunction(x, y)
    
    # Hopefully we hit something before 1000 steps!
    for count in xrange(1000):    
        # Move to the next square
        x = x + direction[0]
        y = y + direction[1]
        tunnelLength = tunnelLength + 1
        
        # We hit something, decide what to do next
        if not genMap.isHigherPriority(x, y, priority): 
            # Handle ending on a pierceable cell
            if genMap.isPierceable(x, y):
                clearCell(curMap, x, y)
                genMap.setTunnel(x, y, True)
                genMap.addJunction(x, y)
                # Remove pierceable walls on the tunnel end also
                genMap.setParallelWallsPierceable(x, y, direction, False)
                print "x,y,direction",x,y,direction
            return x, y, direction
        # Otherwise clear the cell and set the parallel walls as pierceable
        clearCell(curMap, x, y)
        genMap.setTunnel(x, y, True)
        # /todo the turning algorithm sets some internal walls in the tunnel as pierceable
        # is this ok?  Need to think about it.
        genMap.setParallelWallsPierceable(x, y, direction, True, priority = priority)
        # Do we turn?
        if (random.randint(1, 100) <= turnPct and tunnelLength > minTunnelLength):
            # Random turning?
            if (random.randint(1, 100) <= randDirectionPct):           
                direction = util.geometry.changeCardinalDirection(direction)
                
            # Turning directed towards target. to-do: don't allow u-turns
            else:
                direction = util.geometry.getCardinalDirection((x, y), target)
            # we just turned
            tunnelLength = 0
    
    # We wandered way too far.
    return(x, y, direction)
        
        
## Return a random cell in the range ([1, width - 2], [1, height - 2])
def randomCell(curMap, width, height):
    x = random.randint(1, width - 2)
    y = random.randint(1, height - 2)
    return curMap.getContainer((x, y))
    

## Given a partially filled in connection map and a starting point
#  find if all points are connected.  This is a binary check
#  we may want to write a general one to calculate arbitrary transit 
#  times to given locations for monster pathing and sound traveling
#  in the future.
#
#  The basic idea is to start with a list of "old" points and then check to 
#  see if any points are valid and unconnected.  If they are add
#  them to a set of "new" points.  At the beginning of each iteration
#  the new points from the last run become the old points of the current
#  one   
def updateConnectionMap(genMap, connectionMap, start):
    # Make an empty set to chart all the new points and 
    new = set()
    # The starting point is automatically connected
    new.add(start)
    connectionMap[start[0]][start[1]] = True

    while(new):
        old = new.copy()
        new.clear()
        # iterate over cells
        for cell in list(old):
            # iterate over all adjacent cells including diagonals
            for x in xrange(cell[0] - 1, cell[0] + 2):
                for y in xrange(cell[1] - 1, cell[1] + 2):
                    # Bounds check
                    if not genMap.isInBounds(x, y): 
                        continue
                    # Add it if it's addable and hasn't been seen yet
                    if (not connectionMap[x][y]) and (genMap.isRoom(x, y) or genMap.isTunnel(x, y)):
                        connectionMap[x][y] = True
                        new.add((x, y))

    return connectionMap
    

## Look through all the connected rooms and remove those that are connected    
def updateUnconnectedRooms(unconnectedRooms, connectionMap):
    for room in list(unconnectedRooms):
        if connectionMap[room[0]][room[1]]:
            unconnectedRooms.remove(room)
            
    return unconnectedRooms        