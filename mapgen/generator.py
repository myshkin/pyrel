## This module handles regenerating the map of the game world. 
# In fact, much
# of the "map" is preserved whenever a new level is generated -- anything that
# persists will stick around, including e.g. the player, all their items, and
# anything that exists in the GameMap but does not really exist in "reality"
# (e.g. the "object" that handles refreshing the stores every so often).

import container
import gameMap
import generationMap
import genUtility
import things.terrain.terrainLoader
import things.creatures.player
import things.creatures.creatureLoader
import things.creatures.creatureAllocator
import things.items.itemLoader
import things.items.itemAllocator
import util.geometry

import copy
import random


## This routine connects all the rooms in the dungeon (it will be
#  more sophisticated later, for now let's just get it working
#
#  Basic idea is 
def connectRooms(curMap, genMap):
    # Make a new set of the room centers
    unconnectedRooms = genMap.centers.copy()
    # Initialize a flow map to test connectedness
    connectionMap = [ [ False for i in xrange(genMap.height) ] for j in xrange(genMap.width)]
    start = random.choice(list(unconnectedRooms))
    # The start room is automatically connected
    unconnectedRooms.remove(start)
    # Make the flow map starting with the first square
    connectionMap = genUtility.updateConnectionMap(genMap, connectionMap, start)
    # It's possible we've already connected some centers, so remove them
    unconnectedRooms = genUtility.updateUnconnectedRooms(unconnectedRooms, connectionMap)
    # This is the number of times we tried to connect to this target
    tries = 0;
    # Pick a target
    target = random.choice(list(unconnectedRooms))
    
    # Main iteration loop to cycle through all rooms
    while unconnectedRooms and tries < 100:
        tries = tries + 1
        # Make a tunnel
        x, y, direction = genUtility.makeTunnel(curMap, genMap, start, target)
        # Update the connection map and the unconnected rooms
        if genMap.isPierceable(x, y):
            connectionMap = genUtility.updateConnectionMap(genMap, connectionMap, (x, y))
        unconnectedRooms = genUtility.updateUnconnectedRooms(unconnectedRooms, connectionMap)
        # Did we find it?
        if target not in unconnectedRooms:
            # are we done?
            if not unconnectedRooms: 
                continue
            # Get a new target
            target = random.choice(list(unconnectedRooms))
            # Find a nearby connected room center to start from
            start = util.geometry.getNearbyLocation(
                    genMap.centers.difference(unconnectedRooms), target)
            # Nothing else needs to be done, restart the loop
            continue
        # We didn't connect to our target let's find out why
        # Did we end up in a room/tunnel we could enter?
        if genMap.isPierceable(x, y): # to do: allow continuation on tunnels
            start = util.geometry.getNearbyLocation(
                    genMap.centers.difference(unconnectedRooms), target)
            continue
        # did we hit the wall boundary?
        if genMap.isBoundary(x, y):
            # turn around
            direction = util.geometry.reverseDirection(direction)
            x = x + direction[0]
            y = y + direction[1]
            # continue until we hit something that is not a tunnel or room
            while genMap.isTunnel(x, y) or genMap.isRoom(x, y):
                x = x + direction[0]
                y = y + direction[1]
            # did we wind up in a pierceable location?
            if genMap.isPierceable(x, y): 
                continue
        # we're at an obstacle, we need to go around it

        if not genMap.isHigherPriority(x, y, 1):
            #print x, y, direction, target, genMap.grid[x][y].priority
            x, y = tunnelAroundObstacle(curMap, genMap, x, y, direction, target)
            start = (x, y)

    if tries == 100: 
        print "failed on connection"    
        
        
## This routine makes a special tunnel around an obstacle
#  It starts by backing up into the tunnel square and then turning
#  clockwise or CCW.  /todo enable clockwise traversals
def tunnelAroundObstacle(curMap, genMap, xStart, yStart, obstacleDirection, 
        target, priority = 1):
    # backup
    x = xStart - obstacleDirection[0]
    y = yStart - obstacleDirection[1]
    # set entrance to obstacle as a junction
    print "starting tunnel around object at x, y:", x, y
    genMap.addJunction(x, y)
    
    # /todo give option for CW or CCW here.
    # Get the initial tunnel direction
    tunnelDirection = util.geometry.changeCardinalDirection(obstacleDirection, clockwise = 100)
    
    # Limit how many tries we can have
    for tries in xrange(1000):
        # See if we can get in the obstacle (counterclockwise direction forced)
        entranceDirections = genMap.adjacentPierceableObstacle(
                x, y, obstacleDirection, priority = 1)
        if entranceDirections:
            # We can get in!
            entranceLocation = random.choice(list(entranceDirections))
            genUtility.clearCell(curMap, x, y)
            # find what direction we are relative to the other. /todo are diagonal entrances strange?
            direction = (entranceLocation[0] - x, entranceLocation[1] - y)
            # Now move to that square.
            x = entranceLocation[0]
            y = entranceLocation[1]
            genUtility.clearCell(curMap, x, y)
            genMap.setParallelWallsPierceable(x, y, direction, False)
            print "obstacle entrance x, y:", x, y
            return x, y
        # Get the marching square
        newMarchingSquare = genMap.makeMarchingSquare(x, y, obstacleDirection, 
                priority = 1)
        # Find the appropriate directions
        newDirection, exitDirections = util.geometry.marchingSquare(newMarchingSquare)
        
        # Set appropriate pierceable walls.  Because of the possible complex pathing
        # structure, we only set pierceable walls away from the obstacle.
        for dir in exitDirections:               
            genMap.setPierceable(x + dir[0], y + dir[1], True, priority = priority)
            
        # First check if we should exit.  /todo this may fail on complicated convex 
        # structures, so we may need a better algorithm
        targetDirection = util.geometry.getCardinalDirection((x, y), target)
        if targetDirection in exitDirections:
            # first add the current square as a junction
            genMap.addJunction(x, y)
            # move to the new tunnel location
            x += targetDirection[0]
            y += targetDirection[1]
            genUtility.clearCell(curMap, x, y)
            genMap.setParallelWallsPierceable(x, y, targetDirection, True)
            print "leaving obstacle heading towards target"
            print "current location, target", x, y, target
            return x, y
        
        # In the case that we're going straight, we should continue the tunnel
        if newDirection == tunnelDirection:
            x += tunnelDirection[0]
            y += tunnelDirection[1]
            genUtility.clearCell(curMap, x, y)
            #print "going straight:", x, y
            continue
        
        # If we turned CCW it means we've hit an "outer" corner
        # /todo make this work for CW traversals also
        if not util.geometry.isClockwise(tunnelDirection, newDirection):
            # Make the corner square a tunnel
            x += tunnelDirection[0]
            y += tunnelDirection[1]
            genUtility.clearCell(curMap, x, y)
            # Change the obstacle direction 
            obstacleDirection = util.geometry.changeCardinalDirection(obstacleDirection,
                    clockwise = 0)
            tunnelDirection = newDirection
            print "reached outer corner:", x, y    
            continue
            
        # If we turned CW it means we've hit an inner corner
        # in this case we need to just turn the obstacle direction without
        # deleting anything. /todo generalize to either direction
        if util.geometry.isClockwise(tunnelDirection, newDirection):
            obstacleDirection = util.geometry.changeCardinalDirection(obstacleDirection,
                    clockwise = 100)
            tunnelDirection = newDirection
            continue
            
        # The final option is that we're reversed 180, but that should never occur
        raise RuntimeError("tunnelAroundObjects got a reverse direction")        
        
    # If we got this far something horrible has happened, this should probably throw a kill
    # switch and retry making the level.  For now we'll just return the end point to diagnose
    # the level
    return x, y
     
    

    #if tries == 10:
    #    raise RuntimeError("Dungeon generation failed")
    

## Make a debug vault for testing purposes
def makeDebugVault(curMap, genMap, priority = 7):
    width = 50
    height = 40
    #x = random.randint(1, curMap.width - width - 1)
    #y = random.randint(1, curMap.height - height - 1)
    x = 100
    y = 50
    genUtility.makeRectangleHollow(curMap, genMap, x, y, width, height,
            'permanent wall', priority = priority)
    genUtility.makeRectangleFilled(curMap, genMap, x + 1, y + 1,
            width - 2, height - 2, None, priority = priority, isRoom = True)
    # /todo there is still difficulty dealing with connecting up obstacle rooms        
    # set a single pierceable square on the western wall
    #genMap.setPierceable(x, y + (height / 2), True)
    # force the vault to connect up
    #genMap.addCenter(x + 1, y + (height / 2))
    
    
## Make a rectangular room at a possible random location with a 
#  possible random size
#
#  Arguably this is general enough that it should go in genUtility.
#
#  /todo properly handle overwriting old areas for connection purposes
def makeRoom(curMap, genMap, priority, width = 0, height = 0, x = 0, y = 0,
        forcedLocation = False):

    # Get a random width and height, unless specified
    if width == 0:
        width = random.randint(8, 14)
    if height == 0:
        height = random.randint(8, 14)
        
    # Get a random location for the room's northwest corner
    if x == 0:
        x = random.randint(1, curMap.width - width - 1)
    if y == 0:
        y = random.randint(1, curMap.height - height - 1)
    
    # Get another location if this one is bad, try 10 times
    count = 1
    while ((not genUtility.isRectanglePlaceable(genMap, x, y, width, height, priority))
           and (count <= 10)):
            
        # If we really wanted that location, too bad
        if forcedLocation: 
            return 
        # Get a new location
        x = random.randint(1, curMap.width - width - 1)
        y = random.randint(1, curMap.height - height - 1)
        count += 1

    # We failed
    if count == 10: 
        return
        
    # Set the middle square of the room as the center
    genMap.addCenter(x + width / 2, y + height / 2)
        
    # Make the walls outside
    genUtility.makeRectangleHollow(curMap, genMap, x, y, width, height, 
        'granite wall', priority = priority, isPierceable = True)
        
    # Clear the floor
    genUtility.makeRectangleFilled(curMap, genMap, x + 1, y + 1, 
        width - 2, height - 2, None, priority = priority, isRoom = True)


## Place a terrain of a given type in a room
def placeTerrainInRoom(curMap, genMap, feature, targetLevel, priority = 0):
    x, y = genMap.getRandom(isRoom = True)
        
    things.terrain.terrainLoader.makeTerrain(feature, curMap, 
        (x, y), targetLevel)
    genMap.setGridInfo(x, y, priority = priority)
        
    
## Make a "standard" dungeon map.
def makeAngbandLevel(curMap, targetLevel, width, height):

    # Make a new generation map
    genMap = generationMap.GenerationMap(width, height)
    # Make the surrounding wall
    genUtility.makeRectangleHollow(curMap, genMap, 0, 0, 
        width - 1, height - 1, 'permanent wall', priority = 10)
    # Fill the entire map with walls
    genUtility.makeRectangleFilled(curMap, genMap, 1, 1,
        width - 3, height - 3, 'granite wall')

    ## At this point we load the rules that define what features to place
    #  in this archetype.  For the meantime we'll just make some rooms 
    #  and corridors.
    
    ## debug vault for testing pathing
    makeDebugVault(curMap, genMap)
    
    # Make some rooms of priority 1
    for room in xrange(15):
        makeRoom(curMap, genMap, 1)
        
    # Room connection
    connectRooms(curMap, genMap)        

    # Put some stairs into some of the rooms.
    for i in xrange(random.randint(2, 4)):
        placeTerrainInRoom(curMap, genMap, 'down staircase', targetLevel, priority = 8)
    for i in xrange(random.randint(1, 3)):
        placeTerrainInRoom(curMap, genMap, 'up staircase', targetLevel, priority = 8)
            
    # Put the player in somewhere.
    player = curMap.getContainer(container.PLAYERS)[0]
    player.pos = genMap.getRandomCenter()
    print "player.pos",player.pos
    curMap.addSubscriber(player, player.pos)
    
    # Scatter some items about.
    i = 0
    numTries = 0
    itemAllocator = things.items.itemAllocator.ItemAllocator(targetLevel)
    while i < 50 and numTries < 1000:
        cell = genUtility.randomCell(curMap, width, height)
        if not cell:
            itemAllocator.allocate(curMap, cell)
            i += 1
        numTries += 1

    ## Scatter some creatures about.
    i = 0
    numTries = 0
    creatureAllocator = things.creatures.creatureAllocator.CreatureAllocator(targetLevel)
    while i < 10 and numTries < 200:
        cell = genUtility.randomCell(curMap, width, height)
        if not cell:
            creatureAllocator.allocate(curMap, cell.pos)
            i += 1
        numTries += 1




