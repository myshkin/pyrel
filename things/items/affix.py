## Class that stores and applies affix data. Affixes are objects which confer
# or adjust properties on items: changing damage, weight, stat boosts,
# resists etc. They are defined in data/object_affix.txt. Affixes have a type
# and a level, both of which are defined in data/affix_meta.txt. See
# http://bitbucket.org/derakon/pyrel/wiki/data_files for more information.

import allocatorRules
import util.boostedDie
import item
import itemLoader
import itemAllocator
from .. import stats
import util.record

import collections
import random

## Order we want to print out components of the affix record when we serialize.
# None denotes a newline.
# \todo add template fields if implementing affix templates
AFFIX_FIELD_ORDER = ['index', 'affixType', 'position', None,
        'name', 'genus', None,
        'display', None,
        'stats', None,
        'minima', None,
        'mods', None,
        'flags', None,
        'randoms', None,
        'procs', None,
        'conflicts', None,
        'allocations', None,
        'description', None,
]


class Affix:
    ## \param record A dictionary containing our instantiation data.
    def __init__(self, record):
        ## We need to hold onto this
        self.record = record
        ## Names of templates that specify some of our information.
        # \todo Consider affix templates / genera
        self.templates = []
        # Explicit listing of our possible fields, since not all of them
        # are in the record.
        ## Unique identifying integer.
        self.index = None
        ## Broad typing for the affix (e.g. make, material, quality)
        self.affixType = None
        ## Position of affix (either 'prefix' or 'suffix' will affect the
        # item name, anything else will not)
        self.position = None
        ## Name of the affix
        self.name = None
        ## Genus of the affix (generic name for use with multiple affixes)
        self.genus = None
        ## Display metadata
        # \todo Think carefully about allowing affixes to affect display
        self.display = dict()
        ## Stats instance containing all stats for the affix
        self.stats = stats.Stats()
        ## Minimum stats (these are not mods so we don't use a Stats instance)
        self.minima = dict()
        ## List of bonuses.
        self.mods = []
        ## List of flags
        self.flags = []
        ## List of random flag possibilities
        self.randoms = []
        ## ProcFactories that generate the effects the affix is capable of 
        # triggering.
        self.procs = []
        ## List of affixes with which we conflict
        self.conflicts = []
        ## Information on how we allocate ourselves.
        self.allocations = []
        ## The quality level of the affix (determined upon application, since
        # it depends on the item type and itemLevel)
        self.affixLevel = None
        ## Prose description
        self.description = ''

        # Inherit values from our templates first, so they can be overridden
        # by our own attributes as needed.
        completeRecord = {}
#        if 'templates' in self.record:
#            for template in map(itemLoader.getAffixTemplate, self.record['templates']):
#                util.record.applyValues(template.record, completeRecord)
        util.record.applyValues(self.record, completeRecord)

        # Copy values from completeRecord to ourselves.
        for key, value in completeRecord.iteritems():
            setattr(self, key, value)

        # Generate AffixAllocatorRules from our allocation field.
        if 'allocations' in completeRecord:
            self.allocations = [allocatorRules.AffixAllocatorRule(a) for a in completeRecord['allocations']]

        # Fill in our stats.
        self.stats = stats.deserializeStats(completeRecord.get('stats', {}))
        # Apply flags as -1/+1 modifiers, depending on if they begin with a 
        # '-'
        for flag in self.flags:
            if flag.startswith('-'):
                self.stats.addMod(flag, stats.StatMod(0, -1))
            else:
                self.stats.addMod(flag, stats.StatMod(0, 1))

        self.procRecords = completeRecord.get('procs', [])


    ## Functions for applying random properties to items
    # We assume all validity checks have been passed before this
    def applyAffix(self, depth, item):
        # Apply random flags
        for randomFlag in self.randoms:
            flags = []
            # Build a list of available flags from given flag types
            if 'flag types' in randomFlag:
                for flagType in randomFlag['flag types']:
                    for flag in itemLoader.OBJECT_FLAGS:
                        if flag['flagType'] == flagType:
                            flags.append(flag['label'])
            # Add a specific list of flags to the list we're building
            if 'flags' in randomFlag:
                flags.extend(randomFlag['flags'])
            # Now choose from the finished list
            for i in range(randomFlag['number']):
                item.stats.addMod(random.choice(flags), stats.StatMod(0, 1))

        # Apply all other affix stats to item
        item.stats.mergeStats(self.stats)

        # Apply mods to stats
        for mod in self.mods:
            bonus = util.boostedDie.BoostedDie(mod['bonus']).roll()
            statMod = stats.StatMod(0, bonus)
            for flag in mod['flags']:
                item.stats.addMod(flag, statMod)
        # Calculate any modifiers that were in BoostedDie format.
        item.stats.roll(depth)

        # Apply minima
        for stat in self.minima.keys():
            value = item.getStat(stat)
            minimum = int(self.minima[stat])
            if value < minimum:
                item.stats.addMod(stat, stats.StatMod(0, minimum - value))

        # Record affix name, type and level on item for later reference
        item.affixes.append({'name': self.name, 'affixType': self.affixType, 'affixLevel': self.affixLevel})

        # Apply name - we assume that genus and affix positions always match
        genusCounts = collections.Counter()
        # Count the genera of same-position affixes already on the item
        for affixDict in item.affixes:
            affix = itemLoader.getAffix(affixDict['name'])
            if self.position is not None and affix.position == self.position:
                genusCounts[affix.genus] += 1
        # Check for multiple occurrences of the most common genus
        if genusCounts.most_common(1)[0][1] > 1:
            # Use the most common genus
            finalName = genusCounts.most_common(1)[0][0]
        else:
            # Use the affix name if there's no clear winner
            finalName = self.name
        # Apply the chosen name to the item
        if self.position == 'prefix':
            item.namePrefix = finalName
        elif self.position == 'suffix':
            item.nameSuffix = finalName


    ## Serialize us. We use the JSON format, but we output keys in a specific
    # order and try to compact things as much as possible without sacrificing
    # legibility.
    def getSerialization(self):
        return util.record.serializeRecord(self.record, AFFIX_FIELD_ORDER)


    ## Compare us against another Affix, for sorting.
    def __cmp__(self, alt):
        return cmp(self.type, alt.type) or cmp(self.genus, alt.genus)
