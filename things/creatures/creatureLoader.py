## Load the creature_template.txt and creature.txt files.

import creatureFactory
import util.record

import json
import os
import re


## Maps creature name to a factory for that creature.
CREATURE_MAP = dict()
## Generate a creature of the given name at the specified position.
def makeCreature(name, gameMap, pos):
    if name not in CREATURE_MAP:
        raise RuntimeError("Invalid creature name %s" % name)
    CREATURE_MAP[name].makeCreature(gameMap, pos)


## Directly access the factory with the given name.
def getFactory(name):
    if name not in CREATURE_MAP:
        raise RuntimeError("Invalid creature name %s" % name)
    return CREATURE_MAP[name]

## Maps template names to CreatureFactory instances.
TEMPLATE_NAME_MAP = dict()
## Retrieve the appropriately-named template, or None if it doesn't exist.
def getTemplate(label):
    if label not in TEMPLATE_NAME_MAP:
        raise RuntimeError("Invalid creature template name %s" % label)
    return TEMPLATE_NAME_MAP[label]


# First load the creature templates, because they'll be needed when we 
# load the actual creatures.
templates = util.record.loadRecords(
        os.path.join('data', 'creature_template.txt'), 
        creatureFactory.CreatureFactory)
for template in templates:
    TEMPLATE_NAME_MAP[template.name] = template

# Now load the creatures themselves.
creatures = util.record.loadRecords(
        os.path.join('data', 'creature.txt'), creatureFactory.CreatureFactory)
for creature in creatures:
    CREATURE_MAP[creature.name] = creature

#util.record.serializeRecords('creature.txt', CREATURE_MAP.values())
#util.record.serializeRecords('creature_template.txt', TEMPLATE_NAME_MAP.values())
